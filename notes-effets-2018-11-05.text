November 5, 2018
Kenji et Gabriel

But de la journée:
- définir proprement la réduction en profondeur sur notre proposition
  de commandes avec effets.
- si possible, se convaincre que cette réduction est confluente

# Rappel de l'épisode précédent

Dans la préparation de l'exposé HOPE 2018, Kenji et moi avions proposé
une notion de réduction qui fait apparaître des monades dans la
syntaxe (une monade T dans les ensembles (une monade sur la
catégorie Set), appliquée à l'ensemble Com des syntaxes de
commandes possibles):

    ▹ : Rel(Com, T(Com))

Cela permet d'utiliser la monade T(..) pour ajouter des primitives qui
font des effets:

    T(X) := List String × X
    c ::= ... | print str. c
  
    (print str. c) ↦ ([str], c)
  
  
    T(X) := S -> X×S
    c ::= ... | get x. c | set s. c
  
    (get x. c) ▹ (s ↦ (c[s/x], s))
    (set s. c) ▹ (_ ↦ (c, s))

Pour avoir une notion de réduction homogène (parler de clôture
réflexive, transitive etc.) il faut "lifter" cette relation dans une
relation ▹ᵀ : Rel(T(Com), T(Com)), et Kenji et moi avions des idées
raisonnables sur comment faire cela. En gros, ça donne une
construction à la Kleisli, et les effets des réductions successives se
combinent comme on s'y attend:

    (log, (print "foo". print "bar". c)) ▹ᵀ* (log ++ ["foo", "bar"], c)

Par contre, on n'avait pas su faire de la réduction en profondeur dans
ce cadre (c'est pour cela qu'à HOPE je n'ai pas insisté autant qu'on
aimerait sur l'aspect "calcul" de L).

Une idée qui nous avait semblée naturelle à l'époque est de dire: si
on a une réduction en profondeur, on peut utiliser le "map" de la
monade pour ramener l'effet à toplevel. Si vous me permettez le
blasphème d'introduire des contextes à trous C[□], on peut écrire

    c ▹ m
    —————————————————
    C[c] → T(C[_])(m)

où C[_] est vue comme la fonction (c ↦ C[c]) (de Com vers Com), et
T(f) est le "map" de la monade T sur la flèche f (toute monade est un
foncteur, donc a un "map").

Le problème c'est que cette façon de lifter les effets locaux vers des
effets globaux ne marche pas du tout en pratique, elle permet de
séquencer les effets dans un ordre arbitraire et donc de casser la
confluence. Pour

    (log, (print "foo". print "bar". c))

on a en profondeur

    (print "bar" . c) ▹ (["bar"], c)

et donc le lift

    (print "foo" . print "bar" . c) → (["bar"], print "foo" . c)

qui donne lieu à une réduction déplaisante

    (log, print "foo" . print "bar" . c) →ᵀ* (log ++ ["bar", "foo"], c)

Voilà, c'est la fin du résumé de l'épisode précédent. On en était
restés là, "ouais ça marche pas", on a glissé la question sous le
tapis dans nos exposés respectifs, avec la promesse d'honneur de se
revoir un jour pour régler ce problème.


# Commandes avec effets

Commandes avec effets: l'idée est d'avoir une grammaire récursive où
une monade T (sur les ensembles) apparaît pour chaque sous-commande

  TCom ∋   c  := T(Com)
  Com ∋   c₀ ::= < t | e >
  Term ∋   t ::= x | μα.c
  Coterm ∋ e ::= α | μ̃x.c

# Réduction en tête

Réduction des coupures ▹ : Rel(Com, TCom)

  < V | μ̃x.c > ▹ c[V/x]
  < μα.c | S > ▹ c[S/α]

Remarque: à droite, _[P/v] est sur TCom→Tcom, définit par action fonctorielle

  (_[P/v] : TCom→TCom)  =  T(_[P/v] : Com→Com)

Question de comment lifter en une relation Rel(TCom, TCom) ?

On a une spécification d'un lift raisonnable, mais Kenji (qui connaît
les travaux de Shin-Ya) dit que la solution n'est pas toujours
existante ou unique.

  T̂ : forall A B, Rel(A,B) → Rel(TA, TB)
  tel que:
  - return: a R b  =>  ret a T̂R ret b
  - map: ∀(f,g):(R(A,B)→R'(A',B')) (that is, ∀a∀b, a R b => f a R' g b),
     (Tf, Tg) : (T̂R→T̂R')
  - join: m T̂T̂R m'  =>  join m T̂R join m'

Ensuite on définit ▹ᵀ : Rel(TCom, TCom) par Kleisli

  a ▹ᵀ join b  <=>  a T̂▹ b

Question pour plus tard / Shin-Ya / etc.: quel lifting T̂ choisir ?
(cf. Relators: Dal Lago / PBL / Gavazzo)

## Remarque

Si on prend la monade des parties d'un ensemble P(X) = X→2, il
y a beaucoup de lifting qui semblent possibles/raisonnables, par
exemple

  lifting angélique: A P̂(R) B ssi ∃a∈A,∃b∈B, a R b
  lifting démonique: A P̂(R) B ssi ∀a∈A,∀b∈B, a R b
  lifting synchronisé: A P(R) B si ∀a∈A∃b∈B a R b et réciproquement

Le lifting démonique en particulier est un lifting qui admet

  ∅ P̂(R) ∅

et donc

  ∅ ▹ᵀ ∅

À priori, en général, ▹ᵀ n'a pas de raison d'être réflexif.


# Réduction en profondeur

On a défini ▹ : Rel(Com, TCom) et ▹ᵀ : Rel(TCom, TCom).

Maintenant on veut définir → : Rel(Com, TCom) et →ᵀ : Rel(TCom, TCom)
qui font de la réduction en profondeur.

Espoir: si on garde les monades en profondeur dans les sous-termes, on
obtient un calcul confluent. Comme définir la réduction en profondeur ?

On commence par définir des "frames pures" F : TCom -> Com

  F[□] ::= <t|μx.□> | <μα.□|e>

On s'en sert pour définir  →, →ᵀ comme les plus petits points fixes de

  ———————
  ▹ᵀ ⊆ →ᵀ

  c →ᵀ c'
  ————————————
  F[c] → F[c']

  —————————
  T̂(→) ⊆ →ᵀ

Ensuite on peut définir a posteriori une notion de frames impures et
de contextes de réduction

  Fᵀ ∈ { M : T(Com+1) tq. (c ▹ᵀ c') => (M[c] →ᵀ M[c']) }
  où M[c] est défini par le "map" de la monade
    M[c] := T(plug x)(M)
      where
        plug x (inl c) := c
        plug x (inr 1) := x 

## Remarques

- si T̂(R) admet un point fixe M sur T(Com) (un élément M tel que
  M T̂(R) M), alors ce point fixe est un contexte Fᵀ valide (∅ est un
  point fixe pour le lifting démonique des parties d'un ensemble), et
  on a des dérivations de réduction de la forme

    c →ᵀ c'
    ———————
    M →ᵀ M

  qui effacent la sous-commande qui se réduit. C'est un peu bizarre.

- on a hésité entre deux règles

  ——————— (R1)
  ▹ᵀ ⊆ →ᵀ

et

  c₀ ▹ c
  ———————————— (R2)
  ret c₀ →ᵀ c'

  On peut montrer que R1 implique R2, mais l'inverse ne semble pas
  vrai : R2 ne permet pas de montrer certains comportements qui
  dépendent de la structure de la monade et du lifting.
  
  Dans le cas de la monade des parties d'un ensemble avec le lifting
  démonique, on a ∅ P̂(R) ∅ et donc ∅ ▹ᵀ ∅ et donc ∅ →ᵀ ∅ par R1, mais
  ∅ →ᵀ ∅ n'est pas montrable par R2.
  
  À priori on veut que R1 soit toujours vrai, quelle que soit notre
  définition de ▹ᵀ; avec une autre définition de ▹ᵀ qui relie moins de
  choses (par exemple si on choisit un autre lifting moins clément), il
  est possible que R1 et R2 soient à nouveau équivalentes.

- pour définir les frames impures de façon "sémantique", on prend les
  T(Com+1) qui préservent les réductions comme on veut. On aurait pu
  prendre carrément les (TCom → TCom), mais ça contient beaucoup de
  transformations non-syntaxiques et "profondes", en particulier qui
  inspectent leur entrée. T(Com+1) donne une structure plus proche de
  la syntaxe (le trou 1 peut apparaître aucune fois ou plusieurs, mais
  au moins il est traité paramétriquement).


# Exemples

Id(X) := X
Log(X) := M×X, pour (M,·,ε) monoïde
Err(X) := X+E
State(X) := S → X×S
NDet(X) := Pfin(X)

Liftings:

  Id(R) = R

  (l,a) Log(R) (l, b) ssi a R b

  f State(R) g ssi ∀s,
    f(s) = (a, s')
    g(s) = (b, s')
    a R b

  inl a Err(R) inl b ssi a R b
  inr e Err(R) inr e

  A NDet(R) B ssi:

  - démonique
    ∀a∈A,∀b∈B, a R b
  - angélique
    ∃a∈A,∃b∈B, a R b
  - synchrone
    A Sim(R) B  ∧  B Sim(R⁻¹) A
    avec X Sim(R) Y ssi ∀x∈X,∃y∈Y, x R y

Réductions en tête correspondantes:

  c ▹ c'
  ——————————
  c ▹{Id} c'

  c ▹ (l', c')
  ———————————————————————
  (l, c) ▹{Log} (l·l', c)

  c ▹{Err} c' ssi
  - c = inl a  ∧  a ▹ c', ou
  - c = inr e  ∧  c' = inr e

  f ▹{State} μ(g') ssi
    ∀s,
      f(s) = (c₀, s')
      g(s) = (c', s')
      c₀ ▹ c'

    s' est l'état final après c₀, et aussi l'état initial pour faire
    tourner c', le second layer de g.

Pour NDet, aucun des liftings ne donne la réduction qu'on aimerait
avoir pour le parallélisme. Pour cela il faut se restreindre aux
relations homogènes:

  S∪{a} NDet(R : Rel(A,A)) S∪{b}  ssi  a R b

(Note: pour définir nos Rᵀ on n'a pas besoin des liftings hétérogènes
de T, seulement de liftings homogènes, donc on aurait peut-être dû
travailler avec des Rel(A) plutôt que Rel(A,B) dès le départ; mais en
général le cas hétérogène, plus pauvre, guide mieux l'intuition.)


# Idées pour la confluence

## Preuve pour le lambda-calcul

Idée de preuve de confluence pour le lambda-calcul, inspirée de
Takahashi 1995, "Parallel reductions in λ-calculus": définir la
réduction parallèle, mais de façon astucieuse

  a != λx._
  a => a'
  b => b'
  ———————————— non-redex
  a b => a' b'

  a => a'
  b => b'
  —————————————————————— redex1
  (λx.a) b => (λx.a') b'

  a => a'
  b => b'
  ————————————————————— redex2
  (λx.a) b => a'[b'/x]

Étant donné deux réductions parallèles a => b1, a => b2, on peut faire
l'union de leurs redexes réduits (en remplaçant tous les redex1 par
des redex2 quand ils sont déjà des redex2 de l'autre côté) pour
obtenir facilement

 a => b, b1 => b, b2 => b

## Dans notre cas ?

c = < t | e >
c != < V | μ̃x._ >
c != < μα._ | S >
t => t'
e => e'
————————————————————— non-redex
<t | e> => <t' | e'>

c =>ᵀ c'
S => S'
—————————————————————–———— redex-μ-1
<μα.c | S> => <μα.c' | S'>

c =>ᵀ c'
S => S'
—————————————————————– redex-μ-2
<μα.c | S> => c'[S'/α]

c =>ᵀ c'
V => V'
—————————————————————–———— redex-μ̃-1
<V | μ̃x.c> => <V' | μ̃.c'>

c =>ᵀ c'
V => V'
—————————————————————–———— redex-μ̃-2
<V | μ̃x.c> => c'[V'/x]

Questions:

- quelle est la relation entre => et =>ᵀ ?
- a-t-on besoin de demander quelque chose de
  T̂(=>), une forme d'unionabilité du lifting monadique ?


# Conjectures (à publier un jour ?)

- si on refait cette construction sur le lambda-calcul

  T(Term) ∋ t
  Term ∋ t₀ ::= x | λx.t | t t

  et qu'on prend la monade d'état T(X) = Store → X×Store
  on obtient un calcul isomorphe à Friedman/Felleisen

  Idée: le terme en profondeur (ρ, t) correspond, avec un State(Term)
  pour un état multi-location (State := Loc → Val+1), où ρ est
  interprété comme un state-transformer qui ajoute cet état local
  à l'état global (s ↦ s·ρ):

    (t, s·ρ) ⇓ (V, s')
    ———————————————————
    (ρ, t)(s) = (V, s')

- Lien avec le meta-langage de Moggi ? Espoir: notre théorie
  équationnelle coincide avec celle de Moggi.
